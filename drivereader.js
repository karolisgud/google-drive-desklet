const Soup = imports.gi.Soup;

function DriveReader(a_params){
    this.feedUrl="https://www.googleapis.com/calendar/v3/users/me/calendarList/1?key=";

    this.token = '4/E2XLduNrhArkmMtKmmik28THvVLX.8ukObGDL7iYZsNf4jSVKMpaM0Oy4gwI';
     this.callbacks={
        onError:undefined,
        onNewMail:undefined,
        onNoNewMail:undefined
    };

    this.username=undefined;
    this.password=undefined;

    if (a_params != undefined){
        if (a_params.callbacks!=undefined){
            this.callbacks.onError=a_params.callbacks.onError;
            this.callbacks.onNewMail=a_params.callbacks.onNewMail;
            this.callbacks.onNoNewMail=a_params.callbacks.onNoNewMail;
        }

        this.username='karolisgud';
        this.password='simonavasi17';
    }

    var this_=this;

    try {

        this.atomns = new Namespace('http://purl.org/atom/ns#');

    } catch (e){
        throw 'GmailFeeder: Creating Namespace failed: '+e;
    }

    try {

        this.httpSession = new Soup.SessionAsync();

    } catch (e){
        throw 'GmailFeeder: Creating SessionAsync failed: '+e;
    }

    try {

        Soup.Session.prototype.add_feature.call(this.httpSession, new Soup.ProxyResolverDefault());

    } catch (e){
        throw 'GmailFeeder: Adding ProxyResolverDefault failed: '+e;
    }

    try {

        this.httpSession.connect('authenticate',function(session,msg,auth,retrying,user_data){this_.onAuth(session,msg,auth,retrying,user_data);});

    } catch (e){
        throw 'GmailFeeder: Connecting to authenticate signal failed: '+e;
    }

}

DriveReader.prototype.onAuth = function(session,msg,auth,retrying,user_data){
    if (retrying) {
        if (this.callbacks.onError!=undefined){
            this.callbacks.onError('authFailed');
        }

        return;
    }

    auth.authenticate(this.username,this.password);
}

DriveReader.prototype.check = function() {

    let this_ = this;

    let message = Soup.Message.new('POST', this.feedUrl);
    this.httpSession.queue_message(message, function(session,message){this_.onResponse(session,message)});
}

DriveReader.prototype.onResponse = function(session, message) {
    var atomns=this.atomns;

    this.callbacks.onError(message.response_body.data);
    this.callbacks.onError(message.status_code);

    if (message.status_code!=200){

        if (message.status_code!=401){

            if (this.callbacks.onError!=undefined)
                this.callbacks.onError('feedReadFailed');
        }
        return;
    }

    try {
        this.callbacks.onError('feedReadFailedssss');
        var feed=message.response_body.data;

      /*  feed = feed.replace(/^<\?xml\s+version\s*=\s*(["'])[^\1]+\1[^?]*\?>/, "");
        feed=new XML(feed);

        var newMailsCount=feed.atomns::entry.length();

        if (newMailsCount>0){

            var params={'count':newMailsCount,'messages':[]};

            for (var i=0; i<newMailsCount; i++){
                var entry=feed.atomns::entry[i];
                var message={

                    'title':entry.atomns::title,
                    'summary':entry.atomns::summary,
                    'authorName':entry.atomns::author.atomns::name,
                    'authorEmail':entry.atomns::author.atomns::email,

                };
                params.messages.push(message);
            }

            if (this.callbacks.onNewMail!=undefined)
                this.callbacks.onNewMail(params);
        } else {
            if (this.callbacks.onNoNewMail!=undefined)
                this.callbacks.onNoNewMail();
        }*/
    } catch (e){
        if (this.callbacks.onError!=undefined)
            this.callbacks.onError('feedParseFailed');
    }
}